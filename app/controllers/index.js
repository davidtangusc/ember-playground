import Ember from 'ember';
import Profile from './../models/profile';

export default Ember.Controller.extend({
	title: 'Create a Profile',
	profile: Profile.create(),

	actions: {
		save() {
			this.controllerFor('application').get('items').addObject({
				quantity: 1,
				profile: this.get('profile.content')
			});
			
			this.set('profile', Profile.create())
			this.get('profile').clear();
		}
	}
});
