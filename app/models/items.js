import StorageArray from 'ember-local-storage/session/array';

export default StorageArray.extend({
  storageKey: 'order-items'
});