import Ember from 'ember';

export default Ember.Route.extend({
	model() {
		return [
			{ title: 'Product 1' },
			{ title: 'Product 2' },
			{ title: 'Product 3' }
		];
	},

	actions: {
		addToCart(product) {
			this.controllerFor('application').get('items').addObject(product);
			console.log(product)
		}
	}
});
